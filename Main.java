package application;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class Main extends Application{
    @Override
    public void start(Stage primaryStage){
    
    Group root = new Group();
    Scene scene = new Scene(root, 600,420, Color.rgb(210, 243, 247));

   
   MenuBar menuBar = new MenuBar();

   Menu logo = new Menu("FitnessApp");

   Menu Fitness = new Menu("Fitness");
   MenuItem fitnessItem1 = new MenuItem("Lopen");
   MenuItem fitnessItem2 = new MenuItem("Fietsen");
   MenuItem fitnessItem3 = new MenuItem("Zwemmen");
   MenuItem fitnessItem4 = new MenuItem("Hardlopen");
   Fitness.getItems().addAll(fitnessItem1,fitnessItem2,fitnessItem3,fitnessItem4);

   Menu Groepslessen = new Menu("Groepslessen");
   MenuItem dansen = new MenuItem("Dansen");
   MenuItem wandelen = new MenuItem("Wandelen");
   Groepslessen.getItems().addAll(dansen,wandelen);

   Menu coach = new Menu("Coach");
   MenuItem fts = new MenuItem("Fietscoach");
   MenuItem zwm = new MenuItem("zwemcoach");
   coach.getItems().addAll(fts,zwm);

   Menu lidmaatschap = new Menu("Lidmaatschap");
  
   Menu language = new Menu("Nederlands");
   MenuItem english = new MenuItem("English");
   MenuItem nederlands = new MenuItem("Nederlands");
   language.getItems().addAll(english,nederlands);
 

   menuBar.getMenus().addAll(logo,Fitness,Groepslessen,coach,lidmaatschap,language);


   menuBar.setLayoutX(5);
   menuBar.setLayoutY(5);
 
   root.getChildren().add(menuBar);

      

    primaryStage.setScene(scene);
    primaryStage.show();
    }
    
    public static void main(String [] args){
        
        launch(args);
    }

    }
    



